import sbt._

object Dependencies {
  lazy val scalaTest: Seq[ModuleID] = Seq("org.scalatest" %% "scalatest" % "3.2.2" % Test)

  lazy val parserCombinators: Seq[ModuleID] = Seq("org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2")
}
