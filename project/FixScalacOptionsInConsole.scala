import sbt._

object FixScalacOptionsInConsole extends AutoPlugin {
  import Keys._

  override def requires = plugins.JvmPlugin
  override def trigger  = allRequirements

  override lazy val projectSettings = Seq(
    Compile / console / scalacOptions ~= filter,
    Test / console / scalacOptions ~= filter,
    Compile / doc / scalacOptions ~= filter
  )

  // turn these off for console…
  val removes      = Set("-feature", "-unchecked", "-Xfatal-warnings")
  val removeStarts = Set("-opt:", "-Xlint:", "-Ywarn")

  def filter: Seq[String] => Seq[String] =
    _.filterNot(removes.contains)
      .filterNot(s => removeStarts.exists(s.startsWith))
}
