# Deliveroo Home Assignment

This is toy project built as part of the Deliveroo recruitment process.

Detailed requirements can be found in [PDF](docs/Technical Task - Cron Expression Parser.pdf).


# Build
You will need `docker` and `make`. Run `make build` to compile the project and
build new docker image.

# Run
To run the application, use docker:

```
docker run -it --rm cron-expressions-parser "*/15 0 1,15 * 1-5 /usr/bin/find"
```

# Development
Docker is being used to build the project but for the best development
experience we suggest one shall use `sbt` and her favourite IDE.

