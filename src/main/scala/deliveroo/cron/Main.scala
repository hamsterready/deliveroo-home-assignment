package deliveroo.cron

object Main {

  def main(args: Array[String]): Unit =
    if (args.headOption.isEmpty) {
      println("Usage: program <cron-expression>")
      System.exit(1)
    } else {
      println(render(CronParser.parse(CronParser.cronExpression, args.head)))
    }

  def render(res: CronParser.ParseResult[CronParser.CronExpression]): String =
    res match {
      case CronParser.Success(matched, _) =>
        s"""
           |minute       ${matched.minute.values.mkString(" ")}
           |hour         ${matched.hour.values.mkString(" ")}
           |day of month ${matched.dayOfMonth.values.mkString(" ")}
           |month        ${matched.month.values.mkString(" ")}
           |day of week  ${matched.dayOfWeek.values.mkString(" ")}
           |year         ${matched.maybeYear.fold("no year provided")(_.values.mkString(" "))}
           |command      ${matched.command.cmd}
           |""".stripMargin

      case CronParser.Failure(msg, _) => s"FAILURE: $msg"
      case CronParser.Error(msg, _)   => s"ERROR: $msg"
    }
}
