package deliveroo.cron

import scala.util.parsing.combinator.RegexParsers

/**
 * From man 5 crontab:
 *
 * {{{
 *   The format of a cron command is very much the V7 standard, with a number
 *   of upward-compatible extensions. Each line has five time and date fields,
 *   followed by a command, followed by a new‐ line character ('\n'). The
 *   system crontab (/etc/crontab) uses the same format, except that the
 *   username for the command is specified after the time and date fields and
 *   before the command. The fields may be separated by spaces or tabs. The
 *   maximum permitted length for the command field is 998 characters.
 *
 *   Commands are executed by cron(8) when the minute, hour, and month of year
 *   fields match the current time, and when at least one of the two day
 *   fields (day of month, or day of week) match the current time (see
 *   ``Note'' below). cron(8) examines cron entries once every minute. The
 *   time and date fields are:
 *   field          allowed values
 *   -----          --------------
 *   minute         0-59
 *   hour           0-23
 *   day of month   1-31
 *   month          1-12 (or names, see below)
 *   day of week    0-7 (0 or 7 is Sun, or use names)
 *   A field may be an asterisk (*), which always stands for ``first-last''.
 *
 *   Ranges of numbers are allowed. Ranges are two numbers separated with a
 *   hyphen. The specified range is inclusive. For example, 8-11 for an
 *   ``hours'' entry specifies execution at hours 8, 9, 10 and 11.
 *
 *   Lists are allowed. A list is a set of numbers (or ranges) separated by
 *   commas. Examples: ``1,2,5,9'', ``0-4,8-12''.
 *
 *   Step values can be used in conjunction with ranges. Following a range
 *   with ``/<number>'' specifies skips of the number's value through the
 *   range. For example, ``0-23/2'' can be used in the hours field to specify
 *   command execution every other hour (the alternative in the V7 standard is
 *   ``0,2,4,6,8,10,12,14,16,18,20,22''). Steps are also permitted after an
 *   asterisk, so if you want to say ``every two hours'', just use ``*\/2''.
 *
 *   Names can also be used for the ``month'' and ``day of week'' fields. Use
 *   the first three letters of the particular day or month (case doesn't matter).
 *   Ranges or lists of names are not allowed.
 * }}}
 */
object CronParser extends RegexParsers {

  val RangeMinutes: Range     = Range.inclusive(0, 59)
  val RangeMonths: Range      = Range.inclusive(1, 12)
  val RangeHours: Range       = Range.inclusive(0, 23)
  val RangeDaysOfMonth: Range = Range.inclusive(1, 31)
  val RangeDayOfWeek: Range   = Range.inclusive(0, 7)
  val RangeOfYears: Range     = Range.inclusive(1000, Int.MaxValue)

  final case class FieldExpression(r: Range, values: List[Int])
  final case class CommandExpression(cmd: String)

  /**
   * Cron expression representation
   */
  final case class CronExpression(
    minute: FieldExpression,
    hour: FieldExpression,
    dayOfMonth: FieldExpression,
    month: FieldExpression,
    dayOfWeek: FieldExpression,
    maybeYear: Option[FieldExpression],
    command: CommandExpression
  )

  def number: Parser[Int]                         = """\d+""".r ^^ (_.toInt)
  def const(c: String): Parser[String]            = c
  def dash: Parser[String]                        = const("-")
  def comma: Parser[String]                       = const(",")
  def slash: Parser[String]                       = const("/")
  def asterisk(r: Range): Parser[FieldExpression] = """\*""".r ^^ (_ => FieldExpression(r, r.toList))
  def range(r: Range, itemParser: Parser[FieldExpression]): Parser[FieldExpression] =
    (itemParser ~ dash ~ itemParser).filter {
      case FieldExpression(_, List(a)) ~ _ ~ FieldExpression(_, List(b)) => Range(a, b).forall(r.contains)
    } ^^ {
      case FieldExpression(_, List(a)) ~ _ ~ FieldExpression(_, List(b)) => FieldExpression(r, (a to b).toList)
    }
  def list(r: Range, itemParser: Parser[FieldExpression]): Parser[FieldExpression] =
    (itemParser ~ (comma ~ itemParser).*).map {
      case x ~ xs => (x :: xs.map(_._2)).flatMap(_.values)
    }.filter(_.forall(e => r.contains(e))) ^^ {
      FieldExpression(r, _)
    }
  def step(r: Range, itemParser: Parser[FieldExpression]): Parser[FieldExpression] = {
    val nir = numberInRange(r)
    (asterisk(r) ~ slash ~ nir | range(r, itemParser) ~ slash ~ nir | list(r, itemParser) ~ slash ~ nir) ^^ {
      case FieldExpression(range, values) ~ _ ~ FieldExpression(_, List(n)) =>
        FieldExpression(range, values.filter(_ % n == 0))
    }
  }
  def numberInRange(r: Range): Parser[FieldExpression] =
    number.filter(r.contains).map(n => FieldExpression(r, List(n)))
  def simple(r: Range, i: Parser[FieldExpression]): Parser[FieldExpression] = {
    val nir = numberInRange(r)
    step(r, nir) | step(r, i) | range(r, nir) | range(r, i) | asterisk(r) |
      list(r, range(r, nir) | nir) | list(r, nir) | list(r, i)
  }
  def simpleNumber(r: Range): Parser[FieldExpression] =
    simple(r, numberInRange(r))
  def january: Parser[FieldExpression]   = const("jan") ^^ (_ => FieldExpression(RangeMonths, List(1)))
  def february: Parser[FieldExpression]  = const("feb") ^^ (_ => FieldExpression(RangeMonths, List(2)))
  def march: Parser[FieldExpression]     = const("mar") ^^ (_ => FieldExpression(RangeMonths, List(3)))
  def april: Parser[FieldExpression]     = const("apr") ^^ (_ => FieldExpression(RangeMonths, List(4)))
  def may: Parser[FieldExpression]       = const("may") ^^ (_ => FieldExpression(RangeMonths, List(5)))
  def june: Parser[FieldExpression]      = const("jun") ^^ (_ => FieldExpression(RangeMonths, List(6)))
  def july: Parser[FieldExpression]      = const("jul") ^^ (_ => FieldExpression(RangeMonths, List(7)))
  def august: Parser[FieldExpression]    = const("aug") ^^ (_ => FieldExpression(RangeMonths, List(8)))
  def september: Parser[FieldExpression] = const("sep") ^^ (_ => FieldExpression(RangeMonths, List(9)))
  def october: Parser[FieldExpression]   = const("oct") ^^ (_ => FieldExpression(RangeMonths, List(10)))
  def november: Parser[FieldExpression]  = const("nov") ^^ (_ => FieldExpression(RangeMonths, List(11)))
  def december: Parser[FieldExpression]  = const("dec") ^^ (_ => FieldExpression(RangeMonths, List(12)))
  def monthNames: Parser[FieldExpression] =
    january | february | march | april | may | june | july | august | september | october | november | december
  def month: Parser[FieldExpression]          = simple(RangeMonths, monthNames)
  def sunday: Parser[FieldExpression]         = const("sun") ^^ (_ => FieldExpression(RangeDayOfWeek, List(0)))
  def monday: Parser[FieldExpression]         = const("mon") ^^ (_ => FieldExpression(RangeDayOfWeek, List(1)))
  def tuesday: Parser[FieldExpression]        = const("tue") ^^ (_ => FieldExpression(RangeDayOfWeek, List(2)))
  def wednesday: Parser[FieldExpression]      = const("wed") ^^ (_ => FieldExpression(RangeDayOfWeek, List(3)))
  def thursday: Parser[FieldExpression]       = const("thu") ^^ (_ => FieldExpression(RangeDayOfWeek, List(4)))
  def friday: Parser[FieldExpression]         = const("fri") ^^ (_ => FieldExpression(RangeDayOfWeek, List(5)))
  def saturday: Parser[FieldExpression]       = const("sat") ^^ (_ => FieldExpression(RangeDayOfWeek, List(6)))
  def dayOfWeekNames: Parser[FieldExpression] = sunday | monday | tuesday | wednesday | thursday | friday | saturday
  def dayOfWeek: Parser[FieldExpression]      = simple(RangeDayOfWeek, dayOfWeekNames)
  def command: Parser[CommandExpression]      = """.+""".r ^^ CommandExpression

  def cronExpression: Parser[CronExpression] =
    simpleNumber(RangeMinutes) ~ simpleNumber(RangeHours) ~ simpleNumber(RangeDaysOfMonth) ~ month ~ dayOfWeek ~ simpleNumber(
      RangeOfYears
    ).? ~ command ^^ {
      case minute ~ hour ~ dayOfMonth ~ month ~ dayOfWeek ~ maybeYear ~ command =>
        CronExpression(minute, hour, dayOfMonth, month, dayOfWeek, maybeYear, command)
    }
}
