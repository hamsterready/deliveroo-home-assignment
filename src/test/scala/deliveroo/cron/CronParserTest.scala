package deliveroo.cron

import deliveroo.cron.CronParser._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers._

class CronParserTest extends AnyFlatSpec with should.Matchers {
  behavior of "CronParser"

  it should "parse simple cron expressions" in {
    parse(cronExpression, "1 2 3 4 5 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(4)),
      FieldExpression(RangeDayOfWeek, List(5)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse range cron expressions" in {
    parse(cronExpression, "1-3 2-6 3-8 4-6 5-6 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1, 2, 3)),
      FieldExpression(RangeHours, List(2, 3, 4, 5, 6)),
      FieldExpression(RangeDaysOfMonth, List(3, 4, 5, 6, 7, 8)),
      FieldExpression(RangeMonths, List(4, 5, 6)),
      FieldExpression(RangeDayOfWeek, List(5, 6)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse asterisk cron expressions" in {
    parse(cronExpression, "* * * * * cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, (0 to 59).toList),
      FieldExpression(RangeHours, (0 to 23).toList),
      FieldExpression(RangeDaysOfMonth, (1 to 31).toList),
      FieldExpression(RangeMonths, (1 to 12).toList),
      FieldExpression(RangeDayOfWeek, (0 to 7).toList),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse list cron expressions" in {
    parse(cronExpression, "7,14 5,16 7,10 9,10,11 3,2,5 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(7, 14)),
      FieldExpression(RangeHours, List(5, 16)),
      FieldExpression(RangeDaysOfMonth, List(7, 10)),
      FieldExpression(RangeMonths, List(9, 10, 11)),
      FieldExpression(RangeDayOfWeek, List(3, 2, 5)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse step cron expressions" in {
    parse(cronExpression, "*/30 */10 */10 */3 */2 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(0, 30)),
      FieldExpression(RangeHours, List(0, 10, 20)),
      FieldExpression(RangeDaysOfMonth, List(10, 20, 30)),
      FieldExpression(RangeMonths, List(3, 6, 9, 12)),
      FieldExpression(RangeDayOfWeek, List(0, 2, 4, 6)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse step on range cron expressions" in {
    parse(cronExpression, "10-50/30 7-20/10 20-30/10 7-11/3 4-7/2 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(30)),
      FieldExpression(RangeHours, List(10, 20)),
      FieldExpression(RangeDaysOfMonth, List(20, 30)),
      FieldExpression(RangeMonths, List(9)),
      FieldExpression(RangeDayOfWeek, List(4, 6)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse step on list cron expressions" in {
    parse(cronExpression, "0,30/30 10,20/10 20,21,29/10 8,9,11/3 3,4,5,6/2 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(0, 30)),
      FieldExpression(RangeHours, List(10, 20)),
      FieldExpression(RangeDaysOfMonth, List(20)),
      FieldExpression(RangeMonths, List(9)),
      FieldExpression(RangeDayOfWeek, List(4, 6)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse simple months names cron expressions" in {
    parse(cronExpression, "1 2 3 feb 5 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(2)),
      FieldExpression(RangeDayOfWeek, List(5)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse enumerated months names cron expressions" in {
    parse(cronExpression, "1 2 3 feb,jan,aug 5 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(2, 1, 8)),
      FieldExpression(RangeDayOfWeek, List(5)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse ranged months names cron expressions" in {
    parse(cronExpression, "1 2 3 feb-aug 5 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(2, 3, 4, 5, 6, 7, 8)),
      FieldExpression(RangeDayOfWeek, List(5)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse lists and ranged months cron expressions" in {
    parse(cronExpression, "1 2 3 4,10-12 0 command").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(4, 10, 11, 12)),
      FieldExpression(RangeDayOfWeek, List(0)),
      None,
      CommandExpression("command")
    )
  }

  it should "parse step months names cron expressions" in {
    parse(cronExpression, "1 2 3 feb-aug/3 5 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(3, 6)),
      FieldExpression(RangeDayOfWeek, List(5)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse day of week names cron expressions" in {
    parse(cronExpression, "1 2 3 4 tue cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(4)),
      FieldExpression(RangeDayOfWeek, List(2)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse enumerated day of week names cron expressions" in {
    parse(cronExpression, "1 2 3 4 tue,fri cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(4)),
      FieldExpression(RangeDayOfWeek, List(2, 5)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse range of day of week names cron expressions" in {
    parse(cronExpression, "1 2 3 4 tue-fri cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(4)),
      FieldExpression(RangeDayOfWeek, List(2, 3, 4, 5)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse step day of week names cron expressions" in {
    parse(cronExpression, "1 2 3 4 tue-fri/4 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(4)),
      FieldExpression(RangeDayOfWeek, List(4)),
      None,
      CommandExpression("cmd")
    )
  }

  it should "parse year in cron expressions" in {
    parse(cronExpression, "1 2 3 4 tue 1999 cmd").get shouldBe CronExpression(
      FieldExpression(RangeMinutes, List(1)),
      FieldExpression(RangeHours, List(2)),
      FieldExpression(RangeDaysOfMonth, List(3)),
      FieldExpression(RangeMonths, List(4)),
      FieldExpression(RangeDayOfWeek, List(2)),
      Some(FieldExpression(RangeOfYears, List(1999))),
      CommandExpression("cmd")
    )
  }

  it should "fail to parse number out of range (minutes)" in {
    parse(cronExpression, "666 2 3 4 5 cmd").successful shouldBe false
  }

  it should "fail to parse number out of range (hours)" in {
    parse(cronExpression, "1 666 3 4 5 cmd").successful shouldBe false
  }

  it should "fail to parse number out of range (days of month)" in {
    parse(cronExpression, "1 2 666 4 5 cmd").successful shouldBe false
  }

  it should "fail to parse number out of range (months)" in {
    parse(cronExpression, "1 2 3 666 5 cmd").successful shouldBe false
  }

  it should "fail to parse number out of range (days of week)" in {
    parse(cronExpression, "1 2 3 4 666 cmd").successful shouldBe false
  }

  it should "fail to parse number out of range (minutes, list)" in {
    parse(cronExpression, "1,2,3,666 2 3 4 5 cmd").successful shouldBe false
  }

  it should "fail to parse number out of range (minutes, range)" in {
    parse(cronExpression, "3-666 2 3 4 5 cmd").successful shouldBe false
  }

  it should "fail to parse number out of range (minutes, step)" in {
    parse(cronExpression, "3-666/15 2 3 4 5 cmd").successful shouldBe false
  }

  it should "fail to parse number out of range (minutes, step, divisor)" in {
    parse(cronExpression, "3-7/666 2 3 4 5 cmd").successful shouldBe false
  }

}
