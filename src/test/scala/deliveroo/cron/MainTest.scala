package deliveroo.cron

import deliveroo.cron.CronParser._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers._

import scala.util.parsing.input.CharSequenceReader

class MainTest extends AnyFlatSpec with should.Matchers {
  behavior of "Main"

  it should "render cron expressions" in {
    Main.render(
      CronParser.Success(
        CronExpression(
          FieldExpression(RangeMinutes, List(1, 2)),
          FieldExpression(RangeHours, List(2, 3, 4)),
          FieldExpression(RangeDaysOfMonth, List(3, 4, 5)),
          FieldExpression(RangeMonths, List(4, 5, 1)),
          FieldExpression(RangeDayOfWeek, List(5, 6)),
          None,
          CommandExpression("alt-ctrl-delete")
        ),
        new CharSequenceReader("woof")
      )
    ) shouldBe
      """
        |minute       1 2
        |hour         2 3 4
        |day of month 3 4 5
        |month        4 5 1
        |day of week  5 6
        |year         no year provided
        |command      alt-ctrl-delete
        |""".stripMargin
  }

}
