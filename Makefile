build:
	docker build -t cron-expressions-parser .
	echo Try running `docker run -it --rm cron-expressions-parser "*/15 0 1,15 * 1-5 /usr/bin/find"`

run:
	docker run -it --rm cron-expressions-parser "*/15 0 1,15 * 1-5 /usr/bin/find"
