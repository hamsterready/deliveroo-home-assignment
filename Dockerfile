FROM hseeberger/scala-sbt:15.0.2_1.4.7_2.13.4 AS builder
COPY . /app
WORKDIR /app
RUN sbt stage universal:packageBin

FROM openjdk:15-jdk-oraclelinux8
COPY --from=builder /app/target/universal/stage /app
ENTRYPOINT ["/app/bin/cron-expressions-parser"]
